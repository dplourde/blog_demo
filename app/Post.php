<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'title', 'body', 'is_visible',
    ];
    
    
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    
    
    public function scopeVisible($query, $value=true)
    {
        $query->whereIsVisible($value);
    }
    
    public function getRouteKeyName()
    {
        return 'title';
    }
}
