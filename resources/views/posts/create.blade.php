@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form method="post" action="{{ route('posts.store') }}">

                {{ csrf_field() }}

                <input name="title" value="{{ old('title') }}">
                <textarea name="body">{{ old('body') }}</textarea>

                <select name="is_visible">
                    <option value="0">Invisible</option>
                    <option value="1">isible</option>
                </select>

                <button type="submit">Envoyer</button>

            </form>
        </div>
    </div>
</div>

@endsection
