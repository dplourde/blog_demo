<header>
  <div class="blog-masthead">
    <div class="container">
      <nav class="nav">
        <a class="nav-link active" href="/">Home</a>
        <a class="nav-link" href="/posts">Blogue</a>

          @guest
             <a class="nav-link"  href="{{ route('login') }}">Login</a>
             <a class="nav-link"  href="{{ route('register') }}">Register</a>
          @else
              <a class="nav-link"  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                 aria-expanded="false">
                  {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <a class="nav-link"  href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                  Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST"
                    style="display: none;">
                  {{ csrf_field() }}
              </form>
          @endguest
      </nav>
    </div>
  </div>

  <div class="blog-header">
    <div class="container">
      <h1 class="blog-title">The ADN Blog Demo</h1>
      <p class="lead blog-description">An example blog template built with Bootstrap and Laravel.</p>
    </div>
  </div>
</header>